package page.test.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.server.handler.UploadFile;
import org.testng.annotations.AfterTest;

import page.all.common.PropertiesFile;
/**
 * 
 * @author Vengat
 *
 */
public class BaseTestObject {

	protected static WebDriver webDriver = null;
	protected static String uploadFilepath = null;
	
	/**
	 * This block is start working when this class will be loaded
	 */
	static{
		startBrowser();
	}
	
	/**
	 * This method will start the browser and load the URL
	 */
	public static void startBrowser(){
		
		System.setProperty("webdriver.chrome.driver", 
				"D:\\workplace-xoxo\\Engage\\src\\test\\resources\\Driver\\chromedriver.exe");
		uploadFilepath = System.getProperty("user.dir")+"/src/test/resources/File/ImgFeedPost.jpg";
		webDriver = new ChromeDriver();
		resizeTest();
		webDriver.get(PropertiesFile.getResourcesPropertiesData("url"));
	}
	
	public static void resizeTest() {
		webDriver.manage().window().maximize();
	}
	
	@AfterTest
	public void closeBrowser(){
		webDriver.quit();
	}
}

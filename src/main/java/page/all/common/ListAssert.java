package page.all.common;

import java.util.Map;

import org.testng.asserts.*;
import org.testng.collections.Maps;

/**
 * @author Vengat
 *
 */

public class ListAssert extends Assertion {

private Map<AssertionError, IAssert> error_list = Maps.newHashMap();
	

	/*
	 * This method will add all AssertionError in error_list
	 * @see org.testng.asserts.Assertion#executeAssert(org.testng.asserts.IAssert)
	 */
	@Override
	public void executeAssert(IAssert a){
		try{
			a.doAssert();
		}catch(AssertionError ex){
			error_list.put(ex, a);
		}
	}
	
	/*
	 * This method will print all Assertion error
	 */
	public void assertAll(){
		if(!error_list.isEmpty()){
			StringBuilder sb = new StringBuilder
					("The following assert error: \n");
			boolean first = true;
			for(Map.Entry<AssertionError, IAssert> ae: error_list.entrySet()){
				if(first){
					first = false;
				}else{
					sb.append(", ");
				}
				sb.append(ae.getValue().getMessage());
			}
			throw new AssertionError(sb.toString());
		}
	}
}

package page.all.common;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

/**
 * @author Vengat
 */

public class PropertiesFile 
{

	public static Properties prop = null;
	
	static{
		try{
			File file = new File("src/test/resources/File/resource.properties");
			FileInputStream fileInput = new FileInputStream(file);
			prop = new Properties();
			prop.load(fileInput);
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static String getResourcesPropertiesData(String strkey){
		return prop.getProperty(strkey);
	}
}

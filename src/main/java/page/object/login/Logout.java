package page.object.login;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import page.object.common.BasePageObject;


public class Logout extends BasePageObject {

	public Logout(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}
	
	//check whether user icon is displayed or not
	public boolean IsUsernameIcondDisplayed()throws Exception{
		try{
			Reporter.log("Checking whether username icon is displayed", true);
			List<WebElement> user_icon = createElement(UIType.XPATH, 
					".//*[@id='gb']//a[contains(@title,'Google Account')]");
			if(user_icon.get(0).isDisplayed())
				return true;
			else
				return false;
		}catch(Exception e){
			Reporter.log("Failed Method : IsUsernameIcondDisplayed \nReason : " 
									+ e.getMessage(), true);
			throw new Exception(e);
		}
	}
	
	//Clicks the user icon
	public void ClicksUserIcon(boolean trueFalse)throws Exception{
		try{
			Reporter.log("Clicking Username icon", true);
			List<WebElement> user_icon = createElement(UIType.XPATH, 
					".//*[@id='gb']//a[contains(@title,'Google Account')]");
			if(trueFalse){
				user_icon.get(0).click();
				Wait(2000);
			}
		}catch(Exception e){
			Reporter.log("Failed Method : ClicksUserIcon \nReason : " 
										+ e.getMessage(), true);
			throw new Exception(e);
		}
	}

	//check whether sign out is displayed or not
	public boolean IsSignoutButtondDisplayed()throws Exception{
		try{
			Reporter.log("Checking whether signout button is displayed", true);
			List<WebElement> signoutBtn = createElement(UIType.XPATH, 
					".//*[@id='gb_71']");
			if(signoutBtn.get(0).isDisplayed())
				return true;
			else
				return false;
		}catch(Exception e){
			Reporter.log("Failed Method : IsSignoutButtondDisplayed \nReason : " 
									+ e.getMessage(), true);
			throw new Exception(e);
		}
	}
	
	//Clicks the user icon
	public void ClicksSignoutButton(boolean trueFalse)throws Exception{
		try{
			Reporter.log("Clicking Username icon", true);
			List<WebElement> signoutBtn = createElement(UIType.XPATH, 
					".//*[@id='gb_71']");
			if(trueFalse){
				signoutBtn.get(0).click();
				Wait(4000);
			}
		}catch(Exception e){
			Reporter.log("Failed Method : ClicksSignoutButton \nReason : " 
										+ e.getMessage(), true);
			throw new Exception(e);
		}
	}
}

package page.object.login;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import page.object.common.BasePageObject;
/**
 * 
 * @author Vengat
 *
 */
public class Loginpage extends BasePageObject {

	public Loginpage(WebDriver webDriver) {
		super(webDriver);
	}

	//Function check whether Email field is displayed or not
	public boolean IsEmailFieldDisplayed()throws Exception{
		try{
			Reporter.log("Checking whether email field is displayed", true);
			List<WebElement> emailField = createElement(UIType.ID, "Email");
			if(emailField.get(0).isDisplayed())
				return true;
			else
				return false;
		}catch(Exception e){
			Reporter.log("Failed Method : IsEmailFieldDisplayed \nReason : " 
									+ e.getMessage(), true);
			throw new Exception(e);
		}
	}
	
	//Function check whether Next Button is displayed or not
		public boolean IsNextButtonDisplayed()throws Exception{
			try{
				Reporter.log("Checking whether next button is displayed", true);
				List<WebElement> nextBtn = createElement(UIType.ID, "next");
				if(nextBtn.get(0).isDisplayed())
					return true;
				else
					return false;
			}catch(Exception e){
				Reporter.log("Failed Method : IsNextButtonDisplayed \nReason : " 
										+ e.getMessage(), true);
				throw new Exception(e);
			}
		}
	
		//Function check whether password Button is displayed or not
	public boolean IsPasswordFieldDisplayed()throws Exception{
		try{
			Reporter.log("Checking whether password field is displayed", true);
			List<WebElement> passwordField = createElement(UIType.ID, "Passwd");
			if(passwordField.get(0).isDisplayed())
				return true;
			else
				return false;
		}catch(Exception e){
			Reporter.log("Failed Method : IsPasswordFieldDisplayed \nReason : " 
									+ e.getMessage(), true);
			throw new Exception(e);
		}
	}
	
	//Function check whether SignIn Button is displayed or not
	public boolean IsSignInButtonDisplayed()throws Exception{
		try{
			Reporter.log("Checking whether SignIn button is displayed", true);
			List<WebElement> loginbtn = createElement(UIType.ID, "signIn");
			if(loginbtn.get(0).isDisplayed())
				return true;
			else
				return false;
		}catch(Exception e){
			Reporter.log("Failed Method : IsSignInButtonDisplayed \nReason : " 
									+ e.getMessage(), true);
			throw new Exception(e);
		}
	}

	//Function check whether ForgetPassword link is displayed or not
	public boolean IsForgetPasswordDisplayed()throws Exception{
		try{
			Reporter.log("Checking whether forget password is displayed", true);
			List<WebElement> forgetpasswordbtn = createElement(UIType.ID, "link-forgot-passwd");
			if(forgetpasswordbtn.get(0).isDisplayed())
				return true;
			else
				return false;
		}catch(Exception e){
			Reporter.log("Failed Method : IsForgetPasswordDisplayed \nReason : " 
									+ e.getMessage(), true);
			throw new Exception(e);
		}
	}
	
	
	public InboxPage ClickForgetPassword(boolean trueFalse)throws Exception{
		try{
			Reporter.log("Clicking forget password", true);
			List<WebElement> forgetpasswordbtn = createElement(UIType.ID, "link-forgot-passwd");
			if(trueFalse){
				forgetpasswordbtn.get(0).click();
				Wait(2000);
				return new InboxPage(webDriver);
			}
			else 
				return null;
				
		}catch(Exception e){
			Reporter.log("Failed Method : ClickForgetPassword \nReason : " 
									+ e.getMessage(), true);
			throw new Exception(e);
		}
	}
	
	public void EnterEmail(String strEmail)throws Exception{
		try{
			Reporter.log("Entering Email : " + strEmail, true);
			List<WebElement> InpEmail = createElement(UIType.ID, "Email");
			if(InpEmail.get(0).isDisplayed()){
				InpEmail.get(0).clear();
				InpEmail.get(0).sendKeys(strEmail);
				Wait(2000);
			}
		}catch(Exception e){
			Reporter.log("Failed Method : EnterEmail \nReason : " 
										+ e.getMessage(), true);
			throw new Exception(e);
		}
	}
	
	public void EnterPassword(String strPassword)throws Exception{
		try{
			Reporter.log("Entering Password : " + strPassword, true);
			List<WebElement> InpPassword = createElement(UIType.ID, "Passwd");
			if(InpPassword.get(0).isDisplayed()){
				InpPassword.get(0).clear();
				InpPassword.get(0).sendKeys(strPassword);
				Wait(2000);
			}
		}catch(Exception e){
			Reporter.log("Failed Method : EnterPassword \nReason : " 
										+ e.getMessage(), true);
			throw new Exception(e);
		}
	}
	
	public Object ClickLoginButton(boolean trueFalse)throws Exception{
		try{
			Reporter.log("Clicking Login Button ", true);
			List<WebElement> loginbtn = createElement(UIType.ID, "signIn");
			if(trueFalse){
				loginbtn.get(0).click();
				Wait(8000);
				return new Logout(webDriver);
			}else
				return null;
		}catch(Exception e){
			Reporter.log("Failed Method : ClickLoginButton \nReason : " 
										+ e.getMessage(), true);
			throw new Exception(e);
		}
	}
	
	public void ClickNextButton(boolean trueFalse)throws Exception{
		try{
			Reporter.log("Clicking Next Button ", true);
			List<WebElement> nextBtn = createElement(UIType.ID, "next");
			if(trueFalse){
				nextBtn.get(0).click();
				Wait(2000);
			}
		}catch(Exception e){
			Reporter.log("Failed Method : ClickNextButton \nReason : " 
										+ e.getMessage(), true);
			throw new Exception(e);
		}
	}
	
	public boolean IsEmailFieldAlertDisplayed()throws Exception{
		try{
			Reporter.log("Checking whether email field alert is displayed", true);
			List<WebElement> emailAlert = createElement(UIType.XPATH, ".//*[@id='errormsg_0_Email']");
			if(emailAlert.get(0).isDisplayed())
				return true;
			else
				return false;
		}catch(Exception e){
			Reporter.log("Failed Method : IsEmailFieldAlertDisplayed \nReason : " 
									+ e.getMessage(), true);
			throw new Exception(e);
		}
	}
	
	public boolean IsPasswordFieldAlertDisplayed()throws Exception{
		try{
			Reporter.log("Checking whether password field alert is displayed", true);
			List<WebElement> passwordAlert = createElement(UIType.XPATH, ".//*[@id='errormsg_0_Passwd']");
			if(passwordAlert.get(0).isDisplayed())
				return true;
			else
				return false;
		}catch(Exception e){
			Reporter.log("Failed Method : IsPasswordFieldAlertDisplayed \nReason : " 
									+ e.getMessage(), true);
			throw new Exception(e);
		}
	}

	public boolean IsBackArrowDisplayed()throws Exception{
		try{
			Reporter.log("Checking whether back arrow is displayed", true);
			List<WebElement> backArrow = createElement(UIType.ID, "back-arrow");
			if(backArrow.get(0).isDisplayed())
				return true;
			else
				return false;
		}catch(Exception e){
			Reporter.log("Failed Method : IsBackArrowDisplayed \nReason : " 
									+ e.getMessage(), true);
			throw new Exception(e);
		}
	}
	
	public void ClickBackArrow(boolean trueFalse)throws Exception{
		try{
			Reporter.log("Clicking Back Arrow Button ", true);
			List<WebElement> backArrow = createElement(UIType.ID, "back-arrow");
			if(trueFalse){
				backArrow.get(0).click();
				Wait(2000);
			}
		}catch(Exception e){
			Reporter.log("Failed Method : ClickBackArrow \nReason : " 
										+ e.getMessage(), true);
			throw new Exception(e);
		}
	}
}

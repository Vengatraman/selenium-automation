package page.object.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
/**
 * 
 * @author Vengat
 *
 */
public class BasePageObject {
	//variable
	protected WebDriver webDriver;
	
	//constructor
	public BasePageObject(WebDriver webDriver){
		this.webDriver = webDriver;
	}
    
	//Enum
	protected enum UIType{
		ID, CLASS, NAME, TAGNAME, CSS, XPATH, LINK, PARTIALLINK
	}
	
	/**
	 * This method is used for finding an element based upon locating mechanism
	 * 
	 * @param uitype
	 * @param strValue
	 * @return WebElement
	 * 
	 */
	public List<WebElement> createElement(UIType uitype, String strValue){
		if(uitype.name().equalsIgnoreCase("ID")){
			return webDriver.findElements(By.id(strValue));
		}else if(uitype.name().equalsIgnoreCase("CLASS")){
			return webDriver.findElements(By.className(strValue));
		}else if(uitype.name().equalsIgnoreCase("NAME")){
			return webDriver.findElements(By.name(strValue));
		}else if(uitype.name().equalsIgnoreCase("TAGNAME")){
			return webDriver.findElements(By.tagName(strValue));
		}else if(uitype.name().equalsIgnoreCase("CSS")){
			return webDriver.findElements(By.cssSelector(strValue));
		}else if(uitype.name().equalsIgnoreCase("XPATH")){
			return webDriver.findElements(By.xpath(strValue));
		}else if(uitype.name().equalsIgnoreCase("LINK")){
			return webDriver.findElements(By.linkText(strValue));
		}else if(uitype.name().equalsIgnoreCase("PARTIALLINK")){
			return webDriver.findElements(By.partialLinkText(strValue));
		}else{
			return null;
		}
	}
	
	/**
	 * This method will be used for wait for an element on page for given time
	 * 
	 * @param timeout
	 * @throws InterruptedException
	 */
	public void Wait(long timeout)throws InterruptedException{
		synchronized (webDriver) {
			webDriver.wait(timeout);
		}
	}
	
}

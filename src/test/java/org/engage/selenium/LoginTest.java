package org.engage.selenium;

import org.testng.Reporter;
import org.testng.annotations.Test;

import page.all.common.ListAssert;
import page.all.common.PropertiesFile;
import page.object.login.InboxPage;
import page.object.login.Loginpage;
import page.object.login.Logout;
import page.test.common.BaseTestObject;

/**
 * @author Vengat
 */
public class LoginTest extends BaseTestObject{

	//objects
	Loginpage loginpage = null;
	InboxPage inboxpage = null;
	Logout logout = null;
	ListAssert Assert = new ListAssert();
	
	//variables
	String url = PropertiesFile.getResourcesPropertiesData("url");
	String email = PropertiesFile.getResourcesPropertiesData("CorrectEmail");
	String password = PropertiesFile.getResourcesPropertiesData("CorrectPassword");
	String InvalidEmail = PropertiesFile.getResourcesPropertiesData("InCorrectEmail");
	String InvalidPassword = PropertiesFile.getResourcesPropertiesData("IncorrectPassword");
	
	
	/**
	 * Login with valid credential
	 * 
	 * @throws Exception
	 */
	@Test(enabled = true, priority = 5)
	public void LoginValidTest()throws Exception{
		try{
			Reporter.log("Starting Login valid Test case", true);
			loginpage = new Loginpage(webDriver);
			Assert.assertTrue(loginpage.IsBackArrowDisplayed(), 
					"BACK ARROW IS NOT DISPLAYED");
			loginpage.ClickBackArrow(true);
			Assert.assertTrue(loginpage.IsEmailFieldDisplayed(), 
					"EMAIL FIELD IS NOT DISPLAYED");
			loginpage.EnterEmail(email);
			Assert.assertTrue(loginpage.IsNextButtonDisplayed(), 
					"NEXT BUTTON IS NOT DISPLAYED");
			loginpage.ClickNextButton(true);
			Assert.assertTrue(loginpage.IsPasswordFieldDisplayed(), 
					"PASSWORD FIELD IS NOT DISPLAYED");
			loginpage.EnterPassword(password);
			Assert.assertTrue(loginpage.IsSignInButtonDisplayed(), 
					"SIGNIN IS NOT DISPLAYED");
			Assert.assertTrue(loginpage.IsForgetPasswordDisplayed(), 
					"FORGET PASSWORD FIELD IS NOT DISPLAYED");
			logout = (Logout) loginpage.ClickLoginButton(true);
			
		}catch(Exception e){
			Reporter.log("Failed test case LoginValidTest", true);
			throw new Exception(e);
		}finally{
			try{
				Assert.assertTrue(logout.IsUsernameIcondDisplayed(),
						"USERNAME ICON IS NOT DISPLAYED");
				logout.ClicksUserIcon(true);
				Assert.assertTrue(logout.IsSignoutButtondDisplayed(),
						"SIGNOUT BUTTON IS NOT DISPLAYED");
				logout.ClicksSignoutButton(true);
				Assert.assertAll();
				Reporter.log("Completed test case LoginValidTest", true);
			}catch(Exception e){
				Reporter.log("Failed test case in finally block: LoginValidTest", true);
				throw new Exception(e);
			}
		}
	}
	
	/**
	 * Login with blank details
	 * 
	 * @throws Exception
	 */
	@Test(enabled = true, priority = 0)
	public void LoginBlankEmailDetailsTest()throws Exception{
		try{
			Reporter.log("Starting Test case LoginBlankEmailDetailsTest", true);
			loginpage = new Loginpage(webDriver);
			webDriver.navigate().refresh();
			Assert.assertTrue(loginpage.IsEmailFieldDisplayed(), 
					"EMAIL FIELD IS NOT DISPLAYED");
			loginpage.ClickNextButton(true);
			Assert.assertTrue(loginpage.IsEmailFieldAlertDisplayed(), 
					"EMAIL FIELD ALERT IS NOT DISPLAYED");
			Assert.assertAll();
			Reporter.log("Completed test case LoginBlankEmailDetailsTest", true);
		}catch(Exception e){
			Reporter.log("Failed test case LoginBlankEmailDetailsTest", true);
			throw new Exception(e);
		}
	}
	
	
	/**
	 * Login with valid Email and blank Password
	 * @throws Exception
	 */
	@Test(enabled = true, priority = 3)
	public void LoginValidEmailBlankPasswordTest()throws Exception{
		try{
			Reporter.log("Starting Test case LoginValidEmailBlankPasswordTest", true);
			loginpage = new Loginpage(webDriver);
			webDriver.navigate().refresh();
			Assert.assertTrue(loginpage.IsEmailFieldDisplayed(), 
					"EMAIL FIELD IS NOT DISPLAYED");
			loginpage.EnterEmail(email);
			loginpage.ClickNextButton(true);
			Assert.assertTrue(loginpage.IsPasswordFieldDisplayed(), 
					"PASSWORD FIELD IS NOT DISPLAYED");
			loginpage.ClickLoginButton(true);
			Assert.assertTrue(loginpage.IsPasswordFieldAlertDisplayed(), 
					"PASSWORD FIELD ALERT IS NOT DISPLAYED");
			Assert.assertAll();
			Reporter.log("Completed Test case LoginValidEmailBlankPasswordTest", true);
		}catch(Exception e){
			Reporter.log("Failed test case LoginValidEmailBlankPasswordTest", true);
			throw new Exception(e);
		}
	}
	
	
	/**
	 * Login with invalid email and valid password
	 * @throws Exception
	 */
	@Test(enabled = true, priority = 2)
	public void LoginInvalidEmailTest()throws Exception{
		try{
			Reporter.log("Starting Test case LoginInvalidEmailTest", true);
			loginpage = new Loginpage(webDriver);
			webDriver.navigate().refresh();
			Assert.assertTrue(loginpage.IsEmailFieldDisplayed(), 
					"EMAIL FIELD IS NOT DISPLAYED");
			loginpage.EnterEmail(InvalidEmail);
			loginpage.ClickNextButton(true);
			Assert.assertTrue(loginpage.IsEmailFieldAlertDisplayed(), 
					"EMAIL FIELD ALERT IS NOT DISPLAYED");
			Assert.assertAll();
			Reporter.log("Completed Test case LoginInvalidEmailTest", true);
		}catch(Exception e){
			Reporter.log("Failed test case LoginInvalidEmailTest", true);
			throw new Exception(e);
		}
	}
	
	/**
	 * Login with valid email and invalid password
	 * 
	 * @throws Exception
	 */
	@Test(enabled = true, priority = 4)
	public void LoginValidEmailInvalidPasswordTest()throws Exception{
		try{
			Reporter.log("Starting Test case LoginValidEmailInvalidPasswordTest", true);
			loginpage = new Loginpage(webDriver);
			webDriver.navigate().refresh();
			Assert.assertTrue(loginpage.IsBackArrowDisplayed(), 
					"BACK ARROW IS NOT DISPLAYED");
			loginpage.ClickBackArrow(true);
			Assert.assertTrue(loginpage.IsEmailFieldDisplayed(), 
					"EMAIL FIELD IS NOT DISPLAYED");
			loginpage.EnterEmail(email);
			loginpage.ClickNextButton(true);
			Assert.assertTrue(loginpage.IsPasswordFieldDisplayed(), 
					"PASSWORD FIELD IS NOT DISPLAYED");
			loginpage.EnterPassword(InvalidPassword);
			Assert.assertTrue(loginpage.IsSignInButtonDisplayed(), 
					"LOGIN BUTTON IS NOT DISPLAYED");
			loginpage.ClickLoginButton(true);
			Assert.assertTrue(loginpage.IsPasswordFieldAlertDisplayed(), 
					"PASSWORD FIELD ALERT IS NOT DISPLAYED");
			Assert.assertAll();
			Reporter.log("Completed Test case LoginValidEmailInvalidPasswordTest", true);
		}catch(Exception e){
			Reporter.log("Failed test case LoginValidEmailInvalidPasswordTest", true);
			throw new Exception(e);
		}
	}
	

	
}